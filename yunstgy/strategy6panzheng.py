# -*- coding: cp936 -*-
'''
`symbol` VARCHAR ( 16) NOT NULL DEFAULT '' ,
`open` DOUBLE NULL DEFAULT NULL,
`lastclose` DOUBLE NULL DEFAULT NULL,
`close` DOUBLE NULL DEFAULT NULL,
`high` DOUBLE NULL DEFAULT NULL,
`low` DOUBLE NULL DEFAULT NULL,
`share` DOUBLE NULL DEFAULT NULL,
`amount` DOUBLE NULL DEFAULT NULL,
`time` VARCHAR ( 12) NOT NULL DEFAULT '' ,
`macd` DOUBLE NULL DEFAULT NULL,
`avg5` DOUBLE NULL DEFAULT NULL,
`avg10` DOUBLE NULL DEFAULT NULL,
`avg20` DOUBLE NULL DEFAULT NULL,
       `avg60` DOUBLE NULL DEFAULT NULL,





===
建仓价修改为了以建仓单日的最高价建仓
'''

import ConfigParser
from mem_stock_data import mem_stock_data
from iniFile import iniFile
from watch import watch
import string,traceback,os,time
from allpl import allpl
from stgy import stgy_enter_6panzheng

def d_to_str(doublestr):
    return "%.2f"%doublestr

#test program
#amem_stock = mem_stock_data()
#dict_mem = amem_stock.get_dict_memdata("test.ini")
#print dict_mem
#mem_stock_data
configname="strategy6panzheng.ini"
class strategy1(object):
    config = ConfigParser.ConfigParser()
    config.readfp(open(configname,"ab+"))
    file_name = config.get("StockParam", "file_name")
    start_time = config.get("StockParam", "start_time")
    end_time = config.get("StockParam", "end_time")
    k2upper=config.getfloat("StockParam", "k2upper") 
    vibper=config.getfloat("StockParam", "vibper") 
    up_down_per = config.getfloat("StockParam", "up_down_per")
    pullback_per = config.getfloat("StockParam", "pullback_per")
    given_close_date = config.get("StockParam", "given_close_date")
    one_buyamount = config.getfloat("StockParam", "one_buyamount")

    try:
        feelv = config.getfloat("StockParam", "feelv")
    except:
        feelv = 0.0005

    stgy_name = config.get("StockParam", "stgy_name")
    
    try:
        dbtype = config.get("StockParam", "dbtype")
    except:
        dbtype = "sqlite"

    owner = "test"
    try:
        owner = config.getfloat("StockParam", "owner")
    except:
        owner = "test"
    allpl=allpl(dbtype,file_name)
    allpl.setowner(owner)

    try:
        allpl.log2dbsql("delete from strategyenter where stgy='"+stgy_name+"';")
    except:
        allpl.logcon2dbsql("delete from strategyenter where stgy='"+stgy_name+"';")
    allpl.log2dbsql("delete from strategypl where stgy='"+stgy_name+"';")
    allpl.log2dbsql("delete from strategyexe where stgy='"+stgy_name+"';")
    mapentersql={}
    mapentermsg={}

    
    
    log_file = open("strategy_exe_log.csv","w")
    str_out_info = "策略名称,"
    str_out_info += "股票代码" + ","
    str_out_info += "开始日期" + ","
    str_out_info += "结束日期" + ","
    str_out_info += "最高价" + ","
    str_out_info += "最高价日期" + ","
    str_out_info += "最低价" + ","
    str_out_info += "最低价日期" + ","
    str_out_info += "建仓价格" + ","
    str_out_info += "建仓日期" + ","
    str_out_info += "出仓价格" + ","
    str_out_info += "出仓日期" + ","
    str_out_info += "最大浮盈价格" + ","
    str_out_info += "最大浮盈日期" + ","
    str_out_info += "最大浮亏价格" + ","
    str_out_info += "最大浮亏日期" + ","
    str_out_info += "maxPL" + ","
    str_out_info += "minPL" + ","
    
    str_out_info += "PL" + "\n"
    log_file.write(str_out_info)

    symboldeq=allpl.getdb("select * from stocks where symbol='sz399300' and time >='"+start_time+"' and time <='"+end_time+"';")
    #print symboldeq
    hs300map={}
    for onek in symboldeq:
        #print onek
        hs300map[onek[0]+onek[8]]=onek[3]
        
    def __init__(self,stockName):
        #mem_stock_data.__init__(self)
        self.last_stock_name = ""
        self.stgyenter=stgy_enter_6panzheng()
        self.symboldeq=[]
        self.reset()
##        self.symboldeq=strategy1.allpl.getdb("select * from strategyselect where stgy='" + strategy1.stgy_name + "' and symbol='" + stockName + "' order by maxtime")
##        print self.symboldeq

        try:
            self.symboldeq=strategy1.allpl.getdb("select * from strategyselect where stgy='" + strategy1.stgy_name + "' and symbol='" + stockName + "' order by maxtime")
        except:
            self.symboldeq=strategy1.allpl.getcondb("select * from strategyselect where stgy='" + strategy1.stgy_name + "' and symbol='" + stockName + "' order by maxtime")


        self.symbolmap=strategy1.allpl.getdbmonthmap("select * from monthk where  symbol='" + stockName + "' order by time",8)
        #print self.symbolmap
        
        

        #liuxw
        self.lastk=[]
        self.lastpos=0
    #if return -1, it will break the data stream
    def on_quote_change(self, stockName, stockOneData):
        self.lastk=stockOneData
        
        if(self.enter(stockName, stockOneData)):
            self.last_stock_name = stockName
            #return -1
            if(self.go_out(stockName, stockOneData)):
                
                #self.log_info()
                self.log_infosql()
                self.reset()
        else:
            self.last_stock_name = stockName
            return 1
        #return -1
#3．	设C区间过后某个价格为P，p=ma10(ma10为10日均线价格)，则建仓        
    def check(self,stock_name,stock_one_data,k2upper,vibper):
        cur_date = stock_one_data[8]
        cur_open=stock_one_data[1]
        last_close=stock_one_data[2]
        cur_close = stock_one_data[3]
        cur_max = stock_one_data[4]
        cur_min = stock_one_data[5]
        cur_ma10 = stock_one_data[11]
        #cur_ma10 = stock_one_data[11]

        if cur_close > 0.001 and len(self.symboldeq) > 0 and len(self.symboldeq[0]) > 7:
##            tmpmonthk=0.0
##            try:
##                monthkk=self.symbolmap[cur_date[0:6]]
##                tmpmonthk=monthkk[12]
##            except:
##                pass 
            #print monthkk
            #print stock_one_data,self.symboldeq
            if self.symboldeq[0][7] > 0.01 and cur_close > self.symboldeq[0][7] and (cur_close -  self.symboldeq[0][7])/self.symboldeq[0][7] <= k2upper:
                #print "now enter++++++++++++++++++++++++++++++++++++++"
                return 1
                        
    def enter(self,stock_name, stock_one_data):
        #change stock, then we reset the global data
        #print stock_one_data
        cur_date = stock_one_data[8]
        cur_open=stock_one_data[1]
        last_close=stock_one_data[2]
        cur_close = stock_one_data[3]
        cur_max = stock_one_data[4]
        cur_min = stock_one_data[5]
        strategy1.allpl.quote_updatemktPL(strategy1.stgy_name,cur_date, stock_name,self.position,self.enter_price, cur_close)
        
            
        if(self.position > 0):
            return 1
        

        
        if self.check(stock_name, stock_one_data,strategy1.k2upper,strategy1.vibper):
            
            self.pullback_max = cur_close
            self.enter_time=cur_date
            self.enter_price=cur_close
            self.out_max=0.0
            self.out_min=9999999.0
            self.out_maxtime=cur_date
            self.out_mintime=cur_date
            self.position = strategy1.one_buyamount/self.enter_price
            strategy1.allpl.enterPL(stock_name,self.position,self.enter_price)
            #self.log_infosqlenter(stock_name)
            if cmp(stock_name,"sh600008")==0:
                print "#############",self.enter_time,self.enter_price,cur_max,cur_min,self.out_max,self.out_maxtime,self.out_min,self.out_mintime
            #if cmp(stock_name,"sz000906")==0:
                #print cur_date,stock_name,self.lowtime,self.hightime,tmpmin2low,strategy1.close2low,self.enter_price,self.low,self.high,self.low/self.high,strategy1.low2high,self.nextn,self.position
        return 0
    
    def go_out(self,stock_name, stock_one_data):
        if self.position == 0:
            return 0

        
        cur_date = stock_one_data[8]
        cur_open=stock_one_data[1]
        cur_close = stock_one_data[3]
        cur_max = stock_one_data[4]
        cur_min = stock_one_data[5]
        if cur_open < 0.0001 and cur_close < 0.001 and cur_max < 0.001 and cur_min < 0.001:
            return 0

        if cur_max > self.out_max:
            self.out_max=cur_max
            self.out_maxtime=cur_date
        if cur_min < self.out_min:
            self.out_min=cur_min
            self.out_mintime=cur_date

        if cmp(stock_name,"sz300038")==0:
            print stock_one_data
            print cur_max,cur_min,self.out_max,self.out_maxtime,self.out_min,self.out_mintime
        
        if(cur_close > self.pullback_max):
               self.pullback_max = cur_close

        if(0 == self.pullback_max):
            pullback = 0
        else:
            pullback = (self.pullback_max - cur_close)/self.pullback_max
        
        if(pullback > strategy1.pullback_per or cur_close < self.enter_price*0.85):
            
            self.out_time = cur_date
            if cur_close < self.enter_price*0.85:
                self.out_price = self.enter_price*0.85
            else:
                self.out_price =cur_close
            strategy1.allpl.updateNetPL(stock_name,(self.out_price-self.enter_price)*self.position, self.position,self.out_price)
            self.lastpos=self.position
            self.position = 0
            return 1
        
        
        #print "123"
        return 0
    def log_infosql(self):
##        if(self.had_logged):
##            return
        msg = []
        msg.append(strategy1.stgy_name)
        msg.append(string.lower(self.last_stock_name))
        msg.append(strategy1.owner)
        msg.append(strategy1.start_time)
        tmpdate=time.strftime('%Y%m%d',time.localtime(time.time()))
        usetmpdate=self.end_time
        if int(self.end_time) > int(tmpdate):
            usetmpdate=tmpdate
        msg.append(usetmpdate)
        
        
        msg.append(self.symboldeq[0][5])
        msg.append(self.symboldeq[0][6])
        msg.append(float(self.symboldeq[0][7]))
        msg.append(self.symboldeq[0][8])
        
        msg.append(float(self.enter_price))
        msg.append(self.enter_time)
        msg.append(float(self.out_price)) 
        msg.append(self.out_time)
        
        msg.append(float(self.out_max))
        msg.append(self.out_maxtime)
        msg.append(float(self.out_min))
        msg.append(self.out_mintime)

        if (self.enter_price < 0.03):
            str_out_info = str_out_info +"0%" + "\n"
        else:
            maxpl = (self.out_max - self.enter_price)/self.enter_price
            minpl = (self.out_min - self.enter_price)/self.enter_price
            netpl = (self.out_price - self.enter_price)/self.enter_price
            
        msg.append(maxpl)
        msg.append(minpl)
        msg.append(netpl)
        hs300up=0.0
        try:#if stocks tingpai then skip
            hs300kb=strategy1.hs300map['sz399300'+self.enter_time]
            hs300ke=strategy1.hs300map['sz399300'+self.out_time]
            hs300up=(hs300ke-hs300kb)/hs300kb
        except:
            print strategy1.stgy_name,self.last_stock_name,"loss sz399300 ",self.enter_time,self.out_time
        msg.append(self.lastpos)
        msg.append(strategy1.feelv)
        msg.append(hs300up)
        #print msg,hs300kb,hs300ke,hs300up
        #self.dblinecount = self.dblinecount + 1
        tmpsql='insert into strategyexe values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'
        strategy1.allpl.log2db(tmpsql, msg)

##        del strategy1.mapentersql[self.last_stock_name]
##        del strategy1.mapentermsg[self.last_stock_name]
        
        return 0

    def log_infosqlenter(self,symbol):
##        if(self.had_logged):
##            return
        msg = []
        msg.append(strategy1.stgy_name)
        msg.append(string.lower(symbol))
        msg.append(strategy1.owner)
        msg.append(strategy1.start_time)
        tmpdate=time.strftime('%Y%m%d',time.localtime(time.time()))
        usetmpdate=self.end_time
        if int(self.end_time) > int(tmpdate):
            usetmpdate=tmpdate
        msg.append(usetmpdate)
        
        
        msg.append(self.symboldeq[0][5])
        msg.append(self.symboldeq[0][6])
        msg.append(float(self.symboldeq[0][7]))
        msg.append(self.symboldeq[0][8])
        
        msg.append(float(self.enter_price))
        msg.append(self.enter_time)
        msg.append(float(self.out_price)) 
        msg.append(self.out_time)
        
        msg.append(float(self.out_max))
        msg.append(self.out_maxtime)
        msg.append(float(self.out_min))
        msg.append(self.out_mintime)

        #liuxw
        testfloatpl=0.0
        if (self.enter_price < 0.03):
            str_out_info = str_out_info +"0%" + "\n"
        else:
            maxpl = (self.out_max - self.enter_price)/self.enter_price
            minpl = (self.out_min - self.enter_price)/self.enter_price
            testfloatpl = (self.lastk[3] - self.enter_price)/self.enter_price
            
        msg.append(maxpl)
        msg.append(minpl)
        msg.append(testfloatpl)
        msg.append(self.lastk[8])
        
        msg.append(0)
        msg.append(0)
        msg.append(self.position)
        msg.append(self.position*self.lastk[3])
        msg.append(strategy1.feelv)
        #self.dblinecount = self.dblinecount + 1
        tmpsql='insert into strategyenter values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'
        #strategy1.allpl.log2db(tmpsql, msg) write to map then last 2 db
        strategy1.mapentersql[symbol]=tmpsql
        strategy1.mapentermsg[symbol]=msg
        
        return 0

    def close(self):
        #print self.lastk
        if abs(self.position) > 0:
            self.log_infosqlenter(self.last_stock_name)   
    def reset(self):
        self.stgyenter.reset()
        self.position = 0
        self.pullback_max = 0.0
        self.out_max = 0.0
        self.out_min = 9999999.9
        self.out_maxtime=""
        self.out_mintime=""
        self.true_up_down = 0.0
        self.had_logged = 0
##        self.t1_open_price = 0.0
##        self.t1_close_price = 0.0
##        self.t1_high_price = 0.0
##        self.t1_high_price_time = ""
##        self.t1_low_price = 0.0
##        self.t1_low_price_time = ""
        self.enter_time = ""
        self.enter_price = 0.0
        self.out_time = ""
        self.out_price = 0.0
        #print "reset ..."
        #traceback.print_stack()
        if len(self.symboldeq) > 0 :
            self.symboldeq.pop(0)
        
        return 0
     
        
    
#test program 
amem_stock = mem_stock_data(configname)
amem_stock.quote_engine(strategy1)

for k,v in strategy1.mapentersql.iteritems():
    strategy1.allpl.log2db(v, strategy1.mapentermsg[k])
    
strategy1.log_file.close()
strategy1.allpl.close()
#amem_stock.close()
#os.system("pause")  



