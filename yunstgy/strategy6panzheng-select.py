# -*- coding: cp936 -*-

# -*- coding: cp936 -*-
'''
`symbol` VARCHAR ( 16) NOT NULL DEFAULT '' ,
`open` DOUBLE NULL DEFAULT NULL,
`lastclose` DOUBLE NULL DEFAULT NULL,
`close` DOUBLE NULL DEFAULT NULL,
`high` DOUBLE NULL DEFAULT NULL,
`low` DOUBLE NULL DEFAULT NULL,
`share` DOUBLE NULL DEFAULT NULL,
`amount` DOUBLE NULL DEFAULT NULL,
`time` VARCHAR ( 12) NOT NULL DEFAULT '' ,
`macd` DOUBLE NULL DEFAULT NULL,
`avg5` DOUBLE NULL DEFAULT NULL,
`avg10` DOUBLE NULL DEFAULT NULL,
`avg20` DOUBLE NULL DEFAULT NULL,
       `avg60` DOUBLE NULL DEFAULT NULL,





===


'''

import ConfigParser
from mem_stock_data import mem_stock_data
from iniFile import iniFile
from watch import watch
import string,traceback,os,time
from allpl import allpl
from stgy import stgy_enter_6panzheng
def d_to_str(doublestr):
    return "%.2f"%doublestr


#test program
#amem_stock = mem_stock_data()
#dict_mem = amem_stock.get_dict_memdata("test.ini")
#print dict_mem
#mem_stock_data
configname="strategy6panzheng-select.ini"
class strategy1(object):
    config = ConfigParser.ConfigParser()
    config.readfp(open(configname,"ab+"))

    file_name = config.get("StockParam", "file_name")
    start_time = config.get("StockParam", "start_time")
    end_time = config.get("StockParam", "end_time")
    k2upper=config.getfloat("StockParam", "k2upper") 
    vibper=config.getfloat("StockParam", "vibper") 
    up_down_per = config.getfloat("StockParam", "up_down_per")
    pullback_per = config.getfloat("StockParam", "pullback_per")
    given_close_date = config.get("StockParam", "given_close_date")
    one_buyamount = config.getfloat("StockParam", "one_buyamount")

    stgy_name=config.get("StockParam", "stgy_name") 
    owner = "test"
    try:
        owner = config.getfloat("StockParam", "owner")
    except:
        owner = "test"
    try:
        dbtype = config.get("StockParam", "dbtype")
    except:
        dbtype = "sqlite"
    allpl=allpl(dbtype,file_name)
    allpl.log2dbsql("delete from strategyselect where stgy='"+stgy_name+"';")

    
    
    log_file = open("strategy_exe_log.csv","w")
    str_out_info = "策略名称,"
    str_out_info += "股票代码" + ","
    str_out_info += "开始日期" + ","
    str_out_info += "结束日期" + ","
    str_out_info += "最高价" + ","
    str_out_info += "最高价日期" + ","
    str_out_info += "最低价" + ","
    str_out_info += "最低价日期" + ","
    str_out_info += "建仓价格" + ","
    str_out_info += "建仓日期" + ","
    str_out_info += "出仓价格" + ","
    str_out_info += "出仓日期" + ","
    str_out_info += "最大浮盈价格" + ","
    str_out_info += "最大浮盈日期" + ","
    str_out_info += "最大浮亏价格" + ","
    str_out_info += "最大浮亏日期" + ","
    str_out_info += "maxPL" + ","
    str_out_info += "minPL" + ","
    
    str_out_info += "PL" + "\n"
    log_file.write(str_out_info)

        
    def __init__(self,stockName):
        #mem_stock_data.__init__(self)
        self.last_stock_name = ""
        self.stgyenter=stgy_enter_6panzheng()
        self.reset()



    #if return -1, it will break the data stream
    def on_quote_change(self, stockName, stockOneData):      
        if(self.enter(stockName, stockOneData)):
            self.last_stock_name = stockName
        else:
            self.last_stock_name = stockName
            return 1
                
    def enter(self,stock_name, stock_one_data):
        #change stock, then we reset the global data
        cur_date = stock_one_data[8]
        cur_open=stock_one_data[1]
        last_close=stock_one_data[2]
        cur_close = stock_one_data[3]
        cur_max = stock_one_data[4]
        cur_min = stock_one_data[5]   
        if self.stgyenter.enter(stock_name, stock_one_data,strategy1.k2upper,strategy1.vibper,0):
            self.log_infosql(cur_date)
            self.reset()

        return 0
    
    def log_infosql(self,selectmonth):
##        if(self.had_logged):
##            return
        msg = []
        msg.append(strategy1.stgy_name)
        msg.append(string.lower(self.last_stock_name))
        msg.append(strategy1.start_time)
        tmpdate=time.strftime('%Y%m%d',time.localtime(time.time()))
        usetmpdate=self.end_time
        if int(self.end_time) > int(tmpdate):
            usetmpdate=tmpdate
        msg.append(usetmpdate)
        
        msg.append(selectmonth)
        msg.append(float(self.stgyenter.high))
        msg.append(self.stgyenter.hightime)
        msg.append(float(self.stgyenter.low))
        msg.append(self.stgyenter.lowtime)
        
        msg.append(0.0)
        msg.append(0.0)
        msg.append(0.0)
        msg.append(0.0)
        
        msg.append(0.0)
        msg.append(0.0)
        msg.append(0.0)
        msg.append(0.0)
        msg.append(strategy1.owner)


        #self.dblinecount = self.dblinecount + 1
        tmpsql='insert into strategyselect values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'
        #print tmpsql
        #strategy1.allpl.log2db(tmpsql, msg)
        try:
            strategy1.allpl.log2db(tmpsql, msg)
        except:
            strategy1.allpl.logconn2db(tmpsql, msg)
        
        return 0

    def close(self):
        beita=0.0 
    def reset(self):
        self.stgyenter.reset()
        self.position = 0
        self.pullback_max = 0.0
        self.out_max = 0.0
        self.out_min = 9999999.9
        self.out_maxtime=""
        self.out_mintime=""
        self.true_up_down = 0.0
        self.had_logged = 0
        self.enter_time = ""
        self.enter_price = 0.0
        self.out_time = ""
        self.out_price = 0.0
        #print "reset ..."
        #traceback.print_stack()
        
        return 0
     
        
    
#test program 
amem_stock = mem_stock_data(configname)
amem_stock.quote_engine(strategy1)
strategy1.log_file.close()
strategy1.allpl.close()
#amem_stock.close()
#os.system("pause")  



